package com.bit.bookflow.libros

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.bit.bookflow.*
import com.bit.bookflow.models.Libross
import com.bit.bookflow.registerlogin.RegisterActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.activity_libros.*
import kotlinx.android.synthetic.main.listado_libros.view.*

class LibrosActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_libros)

        supportActionBar?.title = "Libros"

        verifyUserIsLoggedIn()

        fetchUsers()

    }

    companion object {
        val LIBRO_KEY = "LIBRO_KEY"
    }


    private fun fetchUsers(){
        val ref = FirebaseDatabase.getInstance().getReference("/libros")
        ref.addListenerForSingleValueEvent(object: ValueEventListener{

            override fun onDataChange(p0: DataSnapshot) {
                val adapter = GroupAdapter<GroupieViewHolder>()

                p0.children.forEach {
                    Log.d("Libros", it.toString())
                    val libro = it.getValue(Libross::class.java)
                    if (libro != null){
                        adapter.add(LibroItem(libro))
                    }
                }

                // Al dar click en el libro voy al mismo
                adapter.setOnItemClickListener { item, view ->

                    val libro_Item = item as LibroItem

                    val intent = Intent(view.context, IdLibroActivity::class.java)
                    intent.putExtra(LIBRO_KEY, libro_Item.libros.autor)
                    intent.putExtra(LIBRO_KEY, libro_Item.libros.editorial)
                    intent.putExtra(LIBRO_KEY, libro_Item.libros.nombre)
                    intent.putExtra(LIBRO_KEY, libro_Item.libros.profileImageUrl)
                    intent.putExtra(LIBRO_KEY, libro_Item.libros.uid)
                    intent.putExtra(LIBRO_KEY, libro_Item.libros.idlibros)

                    startActivity(intent)

                    finish()

                }

                recyclerview_libros.adapter = adapter
            }
            override fun onCancelled(p0: DatabaseError) {

            }
        })
    }


    //verifico si el usuario esta logueado
    private fun verifyUserIsLoggedIn(){
        val uid = FirebaseAuth.getInstance().uid
        if (uid == null){
            val intent = Intent(this, RegisterActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or((Intent.FLAG_ACTIVITY_NEW_TASK))
            startActivity(intent)
        }
    }

    //interactuo con el menu
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item?.itemId){
            R.id.menu_agregar -> {

                val intent = Intent(this, AgregarActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or((Intent.FLAG_ACTIVITY_NEW_TASK))
                startActivity(intent)

                //Toast.makeText(this, "ir a agregar_activity", Toast.LENGTH_SHORT).show()
            }
            R.id.menu_buscar -> {

                val intent = Intent(this, BuscarActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or((Intent.FLAG_ACTIVITY_NEW_TASK))
                startActivity(intent)

                //Toast.makeText(this, "ir a buscar_activity", Toast.LENGTH_SHORT).show()
            }
            R.id.menu_nosotros -> {

                val intent = Intent(this, NosotrosActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or((Intent.FLAG_ACTIVITY_NEW_TASK))
                startActivity(intent)

                //Toast.makeText(this, "ir a nosotros_activity", Toast.LENGTH_SHORT).show()
            }
            R.id.menu_ajustes -> {

                val intent = Intent(this, AjustesActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or((Intent.FLAG_ACTIVITY_NEW_TASK))
                startActivity(intent)

                //Toast.makeText(this, "ir a ajustes_activity", Toast.LENGTH_SHORT).show()
            }
            R.id.menu_cerrar -> {
                FirebaseAuth.getInstance().signOut()
                val intent = Intent(this, RegisterActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or((Intent.FLAG_ACTIVITY_NEW_TASK))
                startActivity(intent)
            }
        }

        return super.onOptionsItemSelected(item)
    }

    //muestro el menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.nav_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }
}

// Aquí defino que datos mostrar en el listado de libros
class LibroItem(val libros: Libross): Item<GroupieViewHolder>(){
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {

        viewHolder.itemView.nombre_libro_textview.text = libros.nombre
        Picasso.get().load(libros.profileImageUrl).into(viewHolder.itemView.portada_libro_listado_imageview)
    }

    override fun getLayout(): Int {
        return R.layout.listado_libros
    }
}