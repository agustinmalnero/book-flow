package com.bit.bookflow.libros

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bit.bookflow.R
import com.bit.bookflow.models.Libross
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.activity_id_libro.*
import kotlinx.android.synthetic.main.listado_libros.view.*

class IdLibroActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_id_libro)

        //    val username = intent.getStringExtra(NewMessageActivity.USER_KEY)
        //val libros = intent.getParcelableExtra<Libros>(LibrosActivity.USER_KEY)


        val libro_id = intent.getStringExtra(LibrosActivity.LIBRO_KEY)
        val libros = intent.getParcelableExtra<Libross>(LibrosActivity.LIBRO_KEY)



        supportActionBar?.title = libros?.autor.toString()

        val adapter = GroupAdapter<GroupieViewHolder>()

        adapter.add(IdLibroItem())

        recyclerView_Id_Libro.adapter = adapter


    }
}

class IdLibroItem(): Item<GroupieViewHolder>(){
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {


    }

    override fun getLayout(): Int {
        return R.layout.id_libros_vista
    }
}