package com.bit.bookflow.libros

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bit.bookflow.AjustesActivity
import com.bit.bookflow.NosotrosActivity
import com.bit.bookflow.R
import com.bit.bookflow.models.Libross
import com.bit.bookflow.registerlogin.RegisterActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_agregar.*
import java.util.*

class AgregarActivity : AppCompatActivity() {

    companion object {
        val TAG = "AgregarActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_agregar)

        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true)
        fun onBackPressed(){
            super.onBackPressed()
        }

        // boton de agregar libro
        agregar_button.setOnClickListener {
            performRegister()
        }

        //seleccionar foto
        selectphoto_button_agregar.setOnClickListener {
            Log.d(TAG, "Intento mostrar la selección de foto")

            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 0)
        }


        supportActionBar?.title = "Agregar Libro"

        //verifyUserIsLoggedIn()

    }

    //verifico si el usuario esta logueado
    //private fun verifyUserIsLoggedIn(){
    //    val uid = FirebaseAuth.getInstance().uid
    //    if (uid == null){
    //        val intent = Intent(this, RegisterActivity::class.java)
    //        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or((Intent.FLAG_ACTIVITY_NEW_TASK))
    //        startActivity(intent)
    //    }
    //}

    //interactuo con el menu
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item?.itemId){

            R.id.menu_agregar -> {

                val intent = Intent(this, AgregarActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or((Intent.FLAG_ACTIVITY_NEW_TASK))
                startActivity(intent)

                Toast.makeText(this, "ir a agregar_activity", Toast.LENGTH_SHORT).show()
            }
            R.id.menu_buscar -> {

                val intent = Intent(this, BuscarActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or((Intent.FLAG_ACTIVITY_NEW_TASK))
                startActivity(intent)

                Toast.makeText(this, "ir a buscar_activity", Toast.LENGTH_SHORT).show()
            }
            R.id.menu_nosotros -> {

                val intent = Intent(this, NosotrosActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or((Intent.FLAG_ACTIVITY_NEW_TASK))
                startActivity(intent)

                Toast.makeText(this, "ir a nosotros_activity", Toast.LENGTH_SHORT).show()
            }
            R.id.menu_ajustes -> {

                val intent = Intent(this, AjustesActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or((Intent.FLAG_ACTIVITY_NEW_TASK))
                startActivity(intent)

                Toast.makeText(this, "ir a ajustes_activity", Toast.LENGTH_SHORT).show()
            }
            R.id.menu_cerrar -> {
                FirebaseAuth.getInstance().signOut()
                val intent = Intent(this, RegisterActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or((Intent.FLAG_ACTIVITY_NEW_TASK))
                startActivity(intent)
            }
        }

        return super.onOptionsItemSelected(item)
    }

    //muestro el menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.nav_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }


    var selectedPhotoUri: Uri? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {

            Log.d(TAG,"La foto ha sido seleccionada")

            selectedPhotoUri = data.data

            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, selectedPhotoUri)

            selectphoto_imageview_agregar.setImageBitmap(bitmap)

            selectphoto_button_agregar.alpha = 0f

            //val bitmapDrawable = BitmapDrawable(bitmap)
            //selectphoto_button_agregar.setBackgroundDrawable(bitmapDrawable)
        }
    }

    private fun performRegister(){
        val nombre = nombre_edittext_agregar.text.toString()
        val autor = autor_edittext_agregar.text.toString()
        val editorial = editorial_edittext_agregar.text.toString()

        if (nombre.isEmpty() || autor.isEmpty() || editorial.isEmpty()) {
            Toast.makeText(this, "Por favor, ingrese un texto en: nombre del libro/editorial/autor", Toast.LENGTH_SHORT).show()
            return
        }

        Log.d(TAG, "El nombre del libro es: " + nombre)
        Log.d(TAG, "El autor es: $autor")
        Log.d(TAG, "La editorial es: $editorial")
        uploadImageToFirebaseStorage()
        progress()
    }

    private fun uploadImageToFirebaseStorage() {
        if (selectedPhotoUri == null) return

        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("/libros/$filename")

        ref.putFile(selectedPhotoUri!!)
                .addOnSuccessListener {
                    Log.d(TAG, "Se ha subido la imagen: ${it.metadata?.path}")

                    ref.downloadUrl.addOnSuccessListener {
                        Log.d(TAG, "Dirección del archivo: $it")

                        saveLibroToFirebaseDatabase(it.toString())
                    }
                }
                .addOnFailureListener {
                    Log.d(TAG, "Falla al subir la imagen a la base de datos: ${it.message}")
                }
    }


    private fun progress(){
        findViewById<View>(R.id.progressBar).visibility = View.VISIBLE
    }

    private fun saveLibroToFirebaseDatabase(profileImageUrl: String) {
        val idlibros = UUID.randomUUID().toString()
        val uid = FirebaseAuth.getInstance().uid ?: ""
        val ref = FirebaseDatabase.getInstance().getReference("/libros/$idlibros")

        val libro = Libross(
            idlibros,
            uid,
            nombre_edittext_agregar.text.toString(),
            editorial_edittext_agregar.text.toString(),
            autor_edittext_agregar.text.toString(),
            profileImageUrl
        )



        ref.setValue(libro)
                .addOnSuccessListener {
                    progress()
                    Log.d(TAG, "Finalmente se ha salvado el libro en Firebase Database")

                    //redirigo a libros luego de registrar
                    val intent = Intent(this, LibrosActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                }
                .addOnFailureListener {
                    Log.d(TAG, "Falla en agregar los valores en la base de datos: ${it.message}")
                }
    }



}

//Antes lo agregaba aqui pero luego cree la clase kotlin dentro del package models que se llama Libros y desde allí referencio esto para el resto.
//class Libross(val idlibros: String, val uid: String, val nombre: String, val editorial: String, val autor: String, val profileImageUrl: String){
//    constructor() : this("","","","","","")
//}

