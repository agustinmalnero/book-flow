package com.bit.bookflow

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.bit.bookflow.libros.AgregarActivity
import com.bit.bookflow.libros.BuscarActivity
import com.bit.bookflow.registerlogin.RegisterActivity
import com.google.firebase.auth.FirebaseAuth

class NosotrosActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nosotros)

        supportActionBar?.title = "Acerca de"

        verifyUserIsLoggedIn()

    }

    //verifico si el usuario esta logueado
    private fun verifyUserIsLoggedIn(){
        val uid = FirebaseAuth.getInstance().uid
        if (uid == null){
            val intent = Intent(this, RegisterActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or((Intent.FLAG_ACTIVITY_NEW_TASK))
            startActivity(intent)
        }
    }

    //interactuo con el menu
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item?.itemId){
            R.id.menu_agregar -> {

                val intent = Intent(this, AgregarActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or((Intent.FLAG_ACTIVITY_NEW_TASK))
                startActivity(intent)

                Toast.makeText(this, "ir a agregar_activity", Toast.LENGTH_SHORT).show()
            }
            R.id.menu_buscar -> {

                val intent = Intent(this, BuscarActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or((Intent.FLAG_ACTIVITY_NEW_TASK))
                startActivity(intent)

                Toast.makeText(this, "ir a buscar_activity", Toast.LENGTH_SHORT).show()
            }
            R.id.menu_nosotros -> {

                val intent = Intent(this, NosotrosActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or((Intent.FLAG_ACTIVITY_NEW_TASK))
                startActivity(intent)

                Toast.makeText(this, "ir a nosotros_activity", Toast.LENGTH_SHORT).show()
            }
            R.id.menu_ajustes -> {

                val intent = Intent(this, AjustesActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or((Intent.FLAG_ACTIVITY_NEW_TASK))
                startActivity(intent)

                Toast.makeText(this, "ir a ajustes_activity", Toast.LENGTH_SHORT).show()
            }
            R.id.menu_cerrar -> {
                FirebaseAuth.getInstance().signOut()
                val intent = Intent(this, RegisterActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or((Intent.FLAG_ACTIVITY_NEW_TASK))
                startActivity(intent)
            }
        }

        return super.onOptionsItemSelected(item)
    }

    //muestro el menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.nav_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }
}