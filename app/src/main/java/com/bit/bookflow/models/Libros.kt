package com.bit.bookflow.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Libross(val idlibros: String, val uid: String, val nombre: String, val editorial: String, val autor: String, val profileImageUrl: String): Parcelable {
    constructor() : this("","","","","","")
}